import ast

from rest_framework import serializers

from .models import Teacher, Student, Class, SPECIALIZATIONS


class MultipleChoiceCharField(serializers.MultipleChoiceField):

    def to_representation(self, value):
        if isinstance(value, str):
            value = ast.literal_eval(value)
        return super().to_representation(value)


class ClassSerializer(serializers.ModelSerializer):

    class Meta:
        model = Class
        fields = ('id', 'name', 'description', 'specialization',
                  'teacher', 'students', 'url')


class TeacherSerializer(serializers.ModelSerializer):

    specializations = MultipleChoiceCharField(choices=SPECIALIZATIONS)

    class Meta:
        model = Teacher
        fields = ('id', 'first_name', 'last_name', 'classes',
                  'specializations', 'url')


class StudentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Student
        fields = ('id', 'first_name', 'last_name', 'classes', 'url')
