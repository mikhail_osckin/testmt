from rest_framework.permissions import BasePermission


class TeacherPermission(BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method in ['GET', 'HEAD']:
            return True
        can_change = (request.method in ['PUT', 'DELETE', 'UPDATE']
                      and obj.id == request.user.id)
        return request.user.is_admin or can_change


class StudentPermission(BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method in ['GET', 'HEAD']:
            return True
        is_teacher = (request.method in ['PUT', 'DELETE', 'UPDATE', 'POST']
                      and request.user.is_teacher)
        is_student = (request.method in ['PUT', 'DELETE', 'UPDATE']
                      and request.user.is_student
                      and obj.id == request.user.id)
        return request.user.is_admin or is_teacher or is_student


class ClassPermission(BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method in ['GET', 'HEAD']:
            return True
        elif request.method == 'POST':
            return request.user.is_admin or request.user.is_teacher
        else:
            return (request.user.is_admin
                    or (request.user.is_teacher
                        and obj.teacher.id == request.user.id))
