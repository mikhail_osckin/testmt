import ast

from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager

SPECIALIZATIONS = [
    ('english', 'Английский'),
    ('math', 'Математика'),
    ('stats', 'Статистика'),
]


class User(AbstractBaseUser):
    USERNAME_FIELD = 'first_name'
    objects = BaseUserManager()

    first_name = models.CharField(max_length=20, unique=True)
    last_name = models.CharField(max_length=20)
    is_admin = models.BooleanField(default=False)

    def get_short_name(self):
        return self.first_name

    def get_full_name(self):
        return '{first_name} {last_name}'.format(**vars(self))

    @property
    def is_teacher(self):
        return hasattr(self, 'teacher')

    @property
    def is_student(self):
        return hasattr(self, 'student')


class Teacher(User):
    specializations = models.CharField(choices=SPECIALIZATIONS,
                                       max_length=100)

    @property
    def specializations_set(self):
        return ast.literal_eval(self.specializations)


class Student(User):
    pass


class Class(models.Model):
    name = models.CharField(max_length=20)
    description = models.TextField(blank=True, null=True)
    specialization = models.CharField(choices=SPECIALIZATIONS, max_length=10)
    teacher = models.ForeignKey(Teacher, related_name='classes')
    students = models.ManyToManyField(Student, related_name='classes', blank=True)

    def __repr__(self):
        return self.name
