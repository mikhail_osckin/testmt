import django_filters

from django_filters import rest_framework as filters

from . import models


class BaseFilter(filters.FilterSet):
    id = django_filters.AllValuesMultipleFilter()
    first_name = django_filters.CharFilter()
    last_name = django_filters.CharFilter()
    classes = django_filters.AllValuesMultipleFilter()


class TeacherFilter(BaseFilter):
    specializations = django_filters.ChoiceFilter(choices=models.SPECIALIZATIONS,
                                                  lookup_expr='contains')

    class Meta:
        model = models.Teacher
        fields = ['id', 'first_name', 'last_name', 'classes', 'specializations']


class StudentFilter(BaseFilter):

    class Meta:
        model = models.Student
        fields = ['id', 'first_name', 'last_name', 'classes']


class ClassFilter(filters.FilterSet):
    id = django_filters.AllValuesMultipleFilter()
    name = django_filters.CharFilter()
    specialization = django_filters.ChoiceFilter(choices=models.SPECIALIZATIONS,
                                                 lookup_expr='contains')
    teacher = django_filters.AllValuesFilter(required=False)
    students = django_filters.AllValuesMultipleFilter()

    class Meta:
        model = models.Class
        fields = ['id', 'name', 'teacher', 'students', 'specialization']
