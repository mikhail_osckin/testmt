from rest_framework import viewsets
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated

from . import permissions, filters, models, serializers


class TeacherViewSet(viewsets.ModelViewSet):
    queryset = models.Teacher.objects.all()
    permission_classes = (IsAuthenticated,
                          permissions.TeacherPermission,)
    serializer_class = serializers.TeacherSerializer
    filter_class = filters.TeacherFilter


class StudentViewSet(viewsets.ModelViewSet):
    queryset = models.Student.objects.all()
    permission_classes = (IsAuthenticated,
                          permissions.StudentPermission,)
    serializer_class = serializers.StudentSerializer
    filter_class = filters.StudentFilter


class ClassViewSet(viewsets.ModelViewSet):
    queryset = models.Class.objects.all()
    permission_classes = (IsAuthenticated,
                          permissions.ClassPermission,)
    serializer_class = serializers.ClassSerializer
    filter_class = filters.ClassFilter

    def perform_update(self, serializer):
        data = serializer.validated_data
        if data['specialization'] in data['teacher'].specializations_set:
            return super().perform_update(serializer)
        raise ValidationError('Class specialization differs from Teacher specializations')

    def perform_create(self, serializer):
        data = serializer.validated_data
        if data['specialization'] in data['teacher'].specializations_set:
            return super().perform_create(serializer)
        raise ValidationError('Class specialization differs from Teacher specializations')
