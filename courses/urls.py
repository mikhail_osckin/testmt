# coding=utf-8
from django.conf.urls import url, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'teachers', views.TeacherViewSet)
router.register(r'students', views.StudentViewSet)
router.register(r'classes', views.ClassViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
]
